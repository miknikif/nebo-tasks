from werkzeug.security import generate_password_hash, check_password_hash
from flask_httpauth import HTTPBasicAuth
from datetime import datetime
from platform import node
from typing import Union
from flask import Flask
from os import environ

app = Flask(__name__)
auth = HTTPBasicAuth()
users = {}


def read_env() -> bool:
    """
        Get required env vars and init app state if found anything
    """
    u = environ.get('BASIC_AUTH_LOGINS', '')
    p = environ.get('BASIC_AUTH_PASSWORDS', '')

    if len(u) == 0 or len(p) == 0:
        return False

    users_array = u.split(';;')
    passwords_array = p.split(';;')

    if len(users_array) != len(passwords_array):
        raise Exception('Number of users and password are different, please specify same amount of them!')

    for i, v in enumerate(users_array):
        cur_user = users_array[i]
        cur_password = passwords_array[i]

        if len(cur_user) == 0 or len(cur_password) == 0:
            raise Exception('Empty login/password not permitted! Got: {}: {}, {}'.format(i, cur_user, cur_password))

        users[cur_user] = generate_password_hash(cur_password)
    
    return True
read_env()


@auth.verify_password
def verify_password(username: str, password: str) -> Union[str, bool, None]:
    """
        Function which is going to verify basic auth
    """
    if username in users and \
            check_password_hash(users.get(username), password):
        return username


@app.route("/")
@auth.login_required(optional=bool(len(users) == 0))
def status() -> str:
    """
        Default server route which shows current status
    """
    user = auth.current_user()
    return "Hello, {}!<br />Time is now: {}<br />Running on: {}".format(user if user is not None and len(user) > 0 else 'anonymous', str(datetime.now()), node())


@app.route("/check")
def health_check() -> str:
    """
        Healthcheck route
    """
    return 'OK'


if __name__ == "__main__":
    app.run()
